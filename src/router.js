import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home.vue'
import Register from '@/components/Register.vue'
import Login from '@/components/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      meta: {
        layout: 'AppLayout'
      },
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      meta: {
        layout: 'blankLayout'
      },
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      meta: {
        layout: 'blankLayout'
      },
      component: Register
    }
  ]
})
