import Vue from 'vue'
import App from './App'
import router from './router'
// import store from './store'

import AppLayout from './layouts/AppLayout.vue'
import blankLayout from './layouts/blankLayout.vue'

/* eslint-disable no-new */
// new Vue({
//   el: '#app',
//   router,
//   template: '<App/>',
//   store,
//   components: { App }
// })
Vue.component('AppLayout', AppLayout)
Vue.component('blankLayout', blankLayout)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
